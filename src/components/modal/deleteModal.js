import { useDispatch, useSelector } from "react-redux"
import { Dialog, Typography, Grid, DialogTitle, DialogContent, DialogActions, Button } from "@mui/material";
import { callApiGetAllOrder, confirmDeleteOrderHandler, openToast, setStatusDelete } from "../../actions/order.action";
export const DeleteModal = () =>{
    const dispatch = useDispatch();
    const {orderInfor, statusDelete} = useSelector((reduxData)=>
        reduxData.orderReducer
    )
    const onBtnDeleteHandler = () =>{
        dispatch(confirmDeleteOrderHandler(orderInfor.id));
        dispatch(setStatusDelete(false));
        dispatch(openToast({open: "true",
                            message:"Delete Order Successfully",
                            type:"warning"}));
        dispatch(callApiGetAllOrder())
    }
    const handleClose = () =>{
        dispatch(setStatusDelete(false));
    }
    return(
        <div>
            <Dialog open= {statusDelete} scroll= "paper">  
                    <DialogTitle>
                        <Typography textAlign="center" variant="h6" component="h2">
                            DELETE ORDER
                        </Typography>
                    </DialogTitle>
                    <DialogContent dividers='paper'>
                        <Typography sx ={{mt:2}}>
                            <Grid container spacing={2}>
                                <Grid item >
                                    <p>Xác nhận xóa order có id: {orderInfor.id}</p>
                                </Grid>
                            </Grid>
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button  onClick={handleClose} variant="outlined">Close</Button>
                        <Button onClick={onBtnDeleteHandler} variant="contained" style={{backgroundColor:"#d41616"}}>Delete Order</Button>
                    </DialogActions>
            </Dialog>
        </div>
    )
}